library(tidyverse)
library(viridis)
library(lubridate)
library(zoo)
library(knitr)
library(ggmap)
library(PostcodesioR)
library(sf)
library(stringr)
library(scales)
library(janitor)
library(readxl)
library(plotly)

mh.2021.df <- read.csv("C:/Users/fitzl1/OneDrive - Suffolk County Council/East of England Modelling Fellowship/HES outpatients data extracts/Mental health 2021.csv")
mh.2122.df <- read.csv("C:/Users/fitzl1/OneDrive - Suffolk County Council/East of England Modelling Fellowship/HES outpatients data extracts/Mental health 2122.csv")

mh.age.df <- rbind(mh.2021.df, mh.2122.df) %>%
  mutate(APPTDATE = ymd(APPTDATE))%>% group_by(ageband, APPTDATE) %>% summarise(E12000006 = sum(E12000006))


mh.age.plot <- ggplot(data = mh.age.df %>% filter(CCG_TREATMENT == "06K"), aes(x = APPTDATE, y = E12000006, fill = ageband)) +
  geom_col(position = "stack") + 
  theme_bw() +
  scale_fill_viridis_d() 

ggplotly(mh.age.plot)

mh.ccg.df <- rbind(mh.2021.df, mh.2122.df) %>%
  mutate(APPTDATE = ymd(APPTDATE))%>% group_by(CCG_TREATMENT, APPTDATE) %>% summarise(E12000006 = sum(E12000006))

mh.ccg.plot <- ggplot(data = mh.ccg.df %>% filter(CCG_TREATMENT == "06K"), aes(x = APPTDATE, y = E12000006, fill = CCG_TREATMENT)) +
  geom_col(position = "stack") + 
  theme_bw() +
  scale_fill_viridis_d() 
ggplotly(mh.ccg.plot)
