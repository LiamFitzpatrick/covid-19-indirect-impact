# COVID-19 indirect impact

Interrupted time series modelling to quantify the indirect impacts of COVID-19 on outpatient appointment attendance in the East of England. 

Data come from NHS Hospital Episode Statistics data.

Work conducted as part of the East of England Modelling Fellowship 2021-2022

## Authors and acknowledgements

Liam Fitzpatrick - liam.fitzpatrick@suffolk.gov.uk - lead author

Antoinette Woodhouse - antoinette.woodhouse@dhsc.gov.uk - supervisor

Stefan Scholtes - s.scholtes@jbs.cam.ac.uk - supervisor